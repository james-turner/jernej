import scala.util.matching.Regex
import org.jsoup.Jsoup
import java.io._

import scala.collection.JavaConversions._

object HtmlRequestSender extends App {
  val filename = "championship.csv"

  import scala.io.Source

  val lines = Source.fromInputStream(getClass.getClassLoader.getResourceAsStream(filename)).getLines.toList

  val regex = ".*<title>(^</title>)</title>.*".r
  val ss = "<title>sssss</title>"

  val data = lines map {
    x: String => {
      import org.jsoup.Jsoup
      import org.jsoup.nodes.Document
      val kk = x.split(",").last
      if (kk.startsWith("http")) {
        val doc = Jsoup.connect(x.split(",").last).get

        doc.select("title") map {
          s => {

            println(s)
            s
          }

        }
      } else ""


    }
  }

  val pw = new PrintWriter(new File("hello.txt" ))
  pw.write(data.mkString("\n"))
  pw.close

}