name := "Jernek"

version := "0.1"

scalaVersion := "2.11.12"

val akkaVersion = "2.5.9"
val akkaHttpVersion = "10.1.0"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
  "com.typesafe.akka" %% "akka-stream" % akkaVersion,
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpVersion,
  "org.jsoup" % "jsoup" % "1.8.3"

)

